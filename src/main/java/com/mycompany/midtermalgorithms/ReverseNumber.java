/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.midtermalgorithms;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class ReverseNumber {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        long start, stop;
        start = System.nanoTime();
        int size = kb.nextInt();
        int[] arr = new int[size]; 
        for (int i = 0 ; i<size;i++){
            arr[i]= kb.nextInt();
        }
        System.out.println("Input :");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]+" ");
        }
        
       System.out.println("\nOutput :");
        for (int i = arr.length - 1; i >= 0; i--) {
            System.out.print(arr[i]+" ");
        }
        stop = System.nanoTime();
        System.out.println("\ntime = " + (stop - start) * 1E-9 + "secs.");
    }
    
}
